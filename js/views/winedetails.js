window.StoreView = Backbone.View.extend({

    initialize: function () {
        this.render();
    },

    render: function () {
        $(this.el).html(this.template(this.model.toJSON()));	
        return this;
    },

    events: {
		"ready document"			: "loadMap",
        "change"        : "change",
        "click .save"   : "beforeSave",
        "keyup #location"   : "showMap",
        "click .delete" : "deleteStore",
        "drop #picture" : "dropHandler"
    },
    
	loadMap:function(e){
			var map = new GMaps({
				div: '#mapView',
				lat: 0,
				lng: 52,
				zoom: 13,
				width: '400px',
				height: '300px'
			});
			GMaps.geocode({
			  address: $('#location').val(),
			  callback: function(results, status) {
				if (status == 'OK') {
				  var latlng = results[0].geometry.location;
				  map.setCenter(latlng.lat(), latlng.lng());
				  map.addMarker({
					lat: latlng.lat(),
					lng: latlng.lng()
				  });
				}
			  }
			});
	},
	showMap:function(e){
		if(e.which == 13) {
			var map = new GMaps({
				div: '#mapView',
				lat: 0,
				lng: 52,
				zoom: 13,
				width: '400px',
				height: '300px'
			});
			alert($('#location').val());
			GMaps.geocode({
			  address: $('#location').val(),
			  callback: function(results, status) {
				if (status == 'OK') {
				  var latlng = results[0].geometry.location;
				  map.setCenter(latlng.lat(), latlng.lng());
				  map.addMarker({
					lat: latlng.lat(),
					lng: latlng.lng()
				  });
				}
			  }
			});
			
    	}
	},
	change: function (event) {
        // Remove any existing alert message
        utils.hideAlert();

        // Apply the change to the model
        var target = event.target;
        var change = {};
        change[target.name] = target.value;
        this.model.set(change);

        // Run validation rule (if any) on changed item
        var check = this.model.validateItem(target.id);
        if (check.isValid === false) {
            utils.addValidationError(target.id, check.message);
        } else {
            utils.removeValidationError(target.id);
        }
    },

    beforeSave: function () {
        var self = this;
        var check = this.model.validateAll();
        if (check.isValid === false) {
            utils.displayValidationErrors(check.messages);
            return false;
        }
        // Upload picture file if a new file was dropped in the drop area
        if (this.pictureFile) {
            this.model.set("picture", this.pictureFile.name);
            utils.uploadFile(this.pictureFile,
                function () {
                    self.saveStore();
                }
            );
        } else {
            this.saveStore();
        }
        return false;
    },

    saveStore: function () {
        var self = this;
        this.model.save(null, {
            success: function (model) {
                self.render();
                app.navigate('stores', true);
                utils.showAlert('Success!', 'Store saved successfully', 'alert-success');
            },
            error: function () {
                utils.showAlert('Error', 'An error occurred while trying to delete this item', 'alert-error');
            }
        });
    },

    deleteStore: function () {
        this.model.destroy({
            success: function () {
                window.history.back();
            }
        });
        return false;
    },

    dropHandler: function (event) {
        event.stopPropagation();
        event.preventDefault();
        var e = event.originalEvent;
        e.dataTransfer.dropEffect = 'copy';
        this.pictureFile = e.dataTransfer.files[0];

        // Read the image file from the local file system and display it in the img tag
        var reader = new FileReader();
        reader.onloadend = function () {
            $('#picture').attr('src', reader.result);
        };
        reader.readAsDataURL(this.pictureFile);
    }

});
