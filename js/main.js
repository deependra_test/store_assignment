var AppRouter = Backbone.Router.extend({

    routes: {
        ""                  : "list",
        "stores/page/:page"	: "list",
        "stores/add"         : "addStore",
        "stores/:id"         : "storeDetails",
        "about"             : "about"
    },

    initialize: function () {
        this.headerView = new HeaderView();
        $('.header').html(this.headerView.el);
    },

	list: function(page) {
        var p = page ? parseInt(page, 10) : 1;
        var storeList = new StoreCollection();
        storeList.fetch({success: function(){
            $("#content").html(new StoreListView({model: storeList, page: p}).el);
        }});
        this.headerView.selectMenuItem('home-menu');
    },

    storeDetails: function (id) {
        var store = new Store({id: id});
        store.fetch({success: function(){
            $("#content").html(new StoreView({model: store}).el);
        }});
        this.headerView.selectMenuItem();
    },

	addStore: function() {
        var store = new Store();
        $('#content').html(new StoreView({model: store}).el);
        this.headerView.selectMenuItem('add-menu');
	},

    about: function () {
        if (!this.aboutView) {
            this.aboutView = new AboutView();
        }
        $('#content').html(this.aboutView.el);
        this.headerView.selectMenuItem('about-menu');
    }

});

utils.loadTemplate(['HeaderView', 'StoreView', 'StoreListItemView', 'AboutView'], function() {
    app = new AppRouter();
    Backbone.history.start();

});
