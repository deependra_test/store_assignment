// The in-memory Store. Encapsulates logic to access wine data.
window.store = {

    wines: {},

    populate: function () {

        this.wines[1] = {
            id: 1,
            maplocation:"Kanpur",
			storeHours: "10AM-9PM",
            productRange: "2000-5000",
            description: "Technical product from the store are awesome.",
            picture: "store1.jpg",
            picture1: "store1.jpg",
            picture2: "store2.jpg",
            picture3: "store3.jpg"
        };
        this.wines[2] = {
            id: 2,
            maplocation:"lal bangla kanpur",
			storeHours: "10AM-9PM",
            productRange: "2000-5000",
            description: "Technical product from the store are awesome.",
            picture: "store2.jpg",
            picture1: "store1.jpg",
            picture2: "store2.jpg",
            picture3: "store3.jpg"
        };
        this.wines[3] = {
            id: 3,
           maplocation:"Lucknow",
			storeHours: "10AM-9PM",
            productRange: "2000-5000",
            description: "Technical product from the store are awesome.",
            picture: "store3.jpg",
            picture1: "store1.jpg",
            picture2: "store2.jpg",
            picture3: "store3.jpg"
        };
        this.wines[4] = {
            id: 4,
           maplocation:"Delhi",
			storeHours: "10AM-9PM",
            productRange: "2000-5000",
            description: "Technical product from the store are awesome.",
            picture: "store4.jpg",
            picture1: "store1.jpg",
            picture2: "store2.jpg",
            picture3: "store3.jpg"
        };
        this.wines[5] = {
            id: 5,
            maplocation:"mumbai",
			storeHours: "10AM-9PM",
            productRange: "2000-5000",
            description: "Technical product from the store are awesome.",
            picture: "store5.jpg",
            picture1: "store1.jpg",
            picture2: "store2.jpg",
            picture3: "store3.jpg"
        };
       this.wines[6] = {
            id: 6,
           maplocation:"Lucknow",
			storeHours: "10AM-9PM",
            productRange: "2000-5000",
            description: "Technical product from the store are awesome.",
            picture: "store3.jpg",
            picture1: "store1.jpg",
            picture2: "store2.jpg",
            picture3: "store3.jpg"
        };
        this.wines[7] = {
            id: 7,
           maplocation:"Delhi",
			storeHours: "10AM-9PM",
            productRange: "2000-5000",
            description: "Technical product from the store are awesome.",
            picture: "store4.jpg",
            picture1: "store1.jpg",
            picture2: "store2.jpg",
            picture3: "store3.jpg"
        };
        this.wines[8] = {
            id: 8,
            maplocation:"mumbai",
			storeHours: "10AM-9PM",
            productRange: "2000-5000",
            description: "Technical product from the store are awesome.",
            picture: "store5.jpg",
            picture1: "store1.jpg",
            picture2: "store2.jpg",
            picture3: "store3.jpg"
        };
		this.wines[9] = {
            id: 9,
           maplocation:"Lucknow",
			storeHours: "10AM-9PM",
            productRange: "2000-5000",
            description: "Technical product from the store are awesome.",
            picture: "store3.jpg",
            picture1: "store1.jpg",
            picture2: "store2.jpg",
            picture3: "store3.jpg"
        };
        this.wines[10] = {
            id: 10,
           maplocation:"Delhi",
			storeHours: "10AM-9PM",
            productRange: "2000-5000",
            description: "Technical product from the store are awesome.",
            picture: "store4.jpg",
            picture1: "store1.jpg",
            picture2: "store2.jpg",
            picture3: "store3.jpg"
        };
        this.wines[11] = {
            id: 11,
            maplocation:"mumbai",
			storeHours: "10AM-9PM",
            productRange: "2000-5000",
            description: "Technical product from the store are awesome.",
            picture: "store5.jpg",
            picture1: "store1.jpg",
            picture2: "store2.jpg",
            picture3: "store3.jpg"
        };
		this.wines[12] = {
            id: 12,
           maplocation:"Lucknow",
			storeHours: "10AM-9PM",
            productRange: "2000-5000",
            description: "Technical product from the store are awesome.",
            picture: "store3.jpg",
            picture1: "store1.jpg",
            picture2: "store2.jpg",
            picture3: "store3.jpg"
        };
        this.wines[13] = {
            id: 13,
           maplocation:"Delhi",
			storeHours: "10AM-9PM",
            productRange: "2000-5000",
            description: "Technical product from the store are awesome.",
            picture: "store4.jpg",
            picture1: "store1.jpg",
            picture2: "store2.jpg",
            picture3: "store3.jpg"
        };
        this.wines[14] = {
            id: 14,
            maplocation:"mumbai",
			storeHours: "10AM-9PM",
            productRange: "2000-5000",
            description: "Technical product from the store are awesome.",
            picture: "store5.jpg",
            picture1: "store1.jpg",
            picture2: "store2.jpg",
            picture3: "store3.jpg"
        };
        this.lastId = 24;
    },

    find: function (model) {
        return this.wines[model.id];
    },

    findAll: function () {
        return _.values(this.wines);
    },

    create: function (model) {
        this.lastId++;
        model.set('id', this.lastId);
        this.wines[this.lastId] = model;
        return model;
    },

    update: function (model) {
        this.wines[model.id] = model;
        return model;
    },

    destroy: function (model) {
        delete this.wines[model.id];
        return model;
    }

};

store.populate();

// Overriding Backbone's sync method. Replace the default RESTful services-based implementation
// with a simple in-memory approach.
Backbone.sync = function (method, model, options) {

    var resp;

    switch (method) {
        case "read":
            resp = model.id ? store.find(model) : store.findAll();
            break;
        case "create":
            resp = store.create(model);
            break;
        case "update":
            resp = store.update(model);
            break;
        case "delete":
            resp = store.destroy(model);
            break;
    }

    if (resp) {
        options.success(resp);
    } else {
        options.error("Record not found");
    }
};
