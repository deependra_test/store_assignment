<?php
//echo 'Current PHP version:'. phpversion();

require 'Slim/Slim.php';

$app = new Slim();

$app->get('/stores', 'getStores');
$app->get('/stores/:id',	'getStore');
$app->post('/stores', 'addStore');

$app->run();

function getStores() {
	$sql = "select * FROM store ORDER BY name";
	try {
		$db = getConnection();
		$stmt = $db->query($sql);  
		$stores = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		echo json_encode($stores);
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}

function getStore($id) {
	$sql = "SELECT * FROM store WHERE id=:id";
	try {
		$db = getConnection();
		$stmt = $db->prepare($sql);  
		$stmt->bindParam("id", $id);
		$stmt->execute();
		$store = $stmt->fetchObject();  
		$db = null;
		echo json_encode($store); 
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}

function addStore() {
	$request = Slim::getInstance()->request();
	$store = json_decode($request->getBody());
	$sql = "INSERT INTO store (maplocation, , storeHours, productRange , description, picture,picture1,picture2,picture3) VALUES (:maplocation, :storeHours, :productRange, :description, :picture, :picture1,:picture2,:picture3)";
	try {
		$db = getConnection();
		$stmt = $db->prepare($sql);  
		$stmt->bindParam("maplocation", $store->maplocation);
		$stmt->bindParam("storeHours", $store->storeHours);
		$stmt->bindParam("productRange", $store->productRange);
		$stmt->bindParam("description", $store->description);
		$stmt->bindParam("picture", $store->picture);
		$stmt->bindParam("picture1", $store->picture1);
		$stmt->bindParam("picture2", $store->picture2);
		$stmt->bindParam("picture3", $store->picture3);
		$stmt->execute();
		$store->id = $db->lastInsertId();
		$db = null;
		echo json_encode($store); 
	} catch(PDOException $e) {
		error_log($e->getMessage(), 3, '/var/tmp/php.log');
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}

function getConnection() {
	$dbhost="localhost";
	$dbuser="root";
	$dbpass="";
	$dbname="storedb";
	$dbh = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass);	
	$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	return $dbh;
}

?>
